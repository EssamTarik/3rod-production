-- MySQL dump 10.13  Distrib 5.7.12, for Linux (x86_64)
--
-- Host: localhost    Database: 3rod
-- ------------------------------------------------------
-- Server version	5.7.12-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Admins`
--

DROP TABLE IF EXISTS `Admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admins` (
  `Admin_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Admin_User_Name` varchar(150) DEFAULT NULL,
  `Admin_Password` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`Admin_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='جدول بيانات الادمن للتطبيق';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admins`
--

LOCK TABLES `Admins` WRITE;
/*!40000 ALTER TABLE `Admins` DISABLE KEYS */;
INSERT INTO `Admins` VALUES (1,'mahmoud','123'),(2,'mahmoud','123'),(3,'محمد','45125142'),(4,'gvhbjnk','45125142'),(5,'mahmoud','123'),(6,'الاعغلغعا','123'),(7,'mahmoud_bakr','123'),(8,'mahmoud_bakr','123'),(9,'mahmoud_bakr_hurl','123'),(10,'essam','222'),(11,'mahmoud_bakr_hurl','123'),(12,'essam','222'),(13,'essam','222'),(14,'mahmoud_bakr_hurl','123'),(15,'mahmoud_bakr_hurl','123'),(16,'mahmoud_bakr','123'),(17,'maaasahmoud_bakr','123'),(18,'essam_tarik','123'),(19,'sdfsd','1233'),(20,'fdsfsd','fdsfsdfs');
/*!40000 ALTER TABLE `Admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Bekia`
--

DROP TABLE IF EXISTS `Bekia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bekia` (
  `Bekia_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Bekia_Title` varchar(300) CHARACTER SET utf8 NOT NULL,
  `Bekia_Desc` varchar(800) CHARACTER SET utf8 NOT NULL,
  `Bekia_Date` varchar(250) NOT NULL,
  `Bekia_Time` int(250) NOT NULL,
  `Bekia_Image` varchar(250) CHARACTER SET utf8 NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Price` varchar(250) NOT NULL,
  `Confirm` int(111) NOT NULL,
  PRIMARY KEY (`Bekia_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Bekia`
--

LOCK TABLES `Bekia` WRITE;
/*!40000 ALTER TABLE `Bekia` DISABLE KEYS */;
INSERT INTO `Bekia` VALUES (11,'  hg','bdvv','0117-00-04',2147483647,'58806ff5bd732.png',36,'99',1),(12,'عرض جديد','وصف العرض الجديد','0117-00-00',2147483647,'588d24c0e15a1.png',37,'50',1),(13,'ويو','ويوب','0117-00-00',2147483647,'588d2e496734c.png',37,'5',1),(14,'ة','ة','0117-00-00',2147483647,'588d2ffb11f26.png',37,'8',1),(15,'تتي','بووب','0117-00-00',2147483647,'588d31047d4a4.png',37,'496',1),(16,'ورو','ةيةي','0117-00-00',2147483647,'588d32a9644ec.png',37,'6',0),(17,'تتتتتتتت','وتتتتتت','0117-00-00',2147483647,'588d347c8afb9.png',37,'5',0),(18,'ههههه','ههههه','0117-00-00',2147483647,'588d36ca4e4f4.png',37,'899',0),(19,'عرض بيكيا','عرض بيكيا','0117-00-00',2147483647,'588d39a8aa1e0.png',37,'9999',0),(20,'ممممم','مممم','0117-00-00',2147483647,'588d3abb9a239.png',37,'558',0),(21,'لللللااا','للبببلل','0117-00-00',2147483647,'588d3d1c5b122.png',37,'522',0),(22,'bgcv s. s','sgbsbsb','0117-00-00',2147483647,'588d409c96554.png',37,'999',0),(23,' , z. x','bsbsb','0117-00-00',2147483647,'588d4146dc7b3.png',37,'979',0),(24,'vdbdhhe','gsgdhe','0117-00-00',2147483647,'588d41cf3680d.png',37,'6464',0),(25,'vsvsbsb','gsev','0000-00-00',2147483647,'588d4953c03e0.png',37,'255',0),(26,'vsvsbsb','gsev','0000-00-00',2147483647,'588d496243a77.png',37,'255',0),(27,'bbshs','shshsh','0000-00-00',2147483647,'588d4a136484b.png',37,'6494',0),(28,'dvsvsvdvfe',',. z ','29-Jan-2017',2147483647,'588d6231e1874.png',37,'6465',0),(29,'fhdfhryh','عرض بداية العام','fgbfg',2017,'589549ce388a5.png',20,'5253',1);
/*!40000 ALTER TABLE `Bekia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Bekia_Image`
--

DROP TABLE IF EXISTS `Bekia_Image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bekia_Image` (
  `Bekia_ID` int(11) NOT NULL,
  `Image` varchar(500) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Bekia_Image`
--

LOCK TABLES `Bekia_Image` WRITE;
/*!40000 ALTER TABLE `Bekia_Image` DISABLE KEYS */;
INSERT INTO `Bekia_Image` VALUES (7,'58806685b3621.png'),(7,'588066a70b6bf.png'),(8,'58806a0df2614.png'),(8,'58806a1969a61.png'),(8,'58806a3e2d385.png'),(8,'58806a4a9c4e2.png'),(11,'588070469e5f7.png'),(12,'588d24c3f1cb9.png'),(12,'588d24c72a3d3.png'),(13,'588d2e83bc9f7.png'),(14,'588d30089090c.png'),(15,'588d310d71034.png'),(16,'588d338c9063b.png'),(17,'588d34d2ebf98.png'),(20,'588d3abf68f9a.png'),(20,'588d3c03e70c2.png'),(20,'588d3c0b3d770.png'),(20,'588d3ca6a2e1f.png'),(21,'588d3d1f260fd.png'),(22,'588d40a91e236.png'),(23,'588d414c33877.png'),(23,'588d415079b19.png'),(24,'588d41d84377e.png'),(24,'588d41d84fb09.png'),(24,'588d41db1be61.png'),(25,'588d495fa8c79.png'),(25,'588d495fb4276.png'),(25,'588d4964dfd75.png'),(25,'588d496760478.png'),(25,'588d49695129d.png'),(25,'588d496ebfd02.png'),(27,'588d4a154f56f.png'),(27,'588d4a156cef5.png'),(27,'588d4a1bb237a.png'),(28,'588d62388452b.png'),(28,'588d6238d5a94.png'),(28,'588d623a7369e.png');
/*!40000 ALTER TABLE `Bekia_Image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Catigory`
--

DROP TABLE IF EXISTS `Catigory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Catigory` (
  `Cat_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Cat_Name` varchar(200) NOT NULL,
  PRIMARY KEY (`Cat_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='جدول اقسام المحلات داخل التطبيق';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Catigory`
--

LOCK TABLES `Catigory` WRITE;
/*!40000 ALTER TABLE `Catigory` DISABLE KEYS */;
INSERT INTO `Catigory` VALUES (1,'قسم1'),(2,'قسم2');
/*!40000 ALTER TABLE `Catigory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `News`
--

DROP TABLE IF EXISTS `News`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `News` (
  `News_ID` int(11) NOT NULL AUTO_INCREMENT,
  `News_Title` varchar(20) NOT NULL,
  `News_Desc` varchar(700) NOT NULL,
  `News_Date` date NOT NULL,
  `News_Time` int(11) NOT NULL,
  `Admin_ID` int(11) NOT NULL,
  `News_Image` varchar(800) NOT NULL,
  PRIMARY KEY (`News_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='جدول الاخبار';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `News`
--

LOCK TABLES `News` WRITE;
/*!40000 ALTER TABLE `News` DISABLE KEYS */;
INSERT INTO `News` VALUES (1,'خبر','تفاصيل تفاصيل تفاصيل','2017-01-04',2147483647,4,'586c870e2c7e8.png'),(2,'الخبر الاول','تفاصيل الخبر الاول','2017-01-04',2147483647,1,'586c870e2c7e8.png'),(3,'الخبر الثانى','تفاصيل الخبر التانى','2017-01-11',2147483647,1,'586c870e2c7e8.png');
/*!40000 ALTER TABLE `News` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Post_Images`
--

DROP TABLE IF EXISTS `Post_Images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Post_Images` (
  `Post_ID` int(11) NOT NULL,
  `Image` text CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Post_Images`
--

LOCK TABLES `Post_Images` WRITE;
/*!40000 ALTER TABLE `Post_Images` DISABLE KEYS */;
INSERT INTO `Post_Images` VALUES (49,'586c87107f773.png'),(49,'586c8712b6804.png'),(49,'586c87130bbf6.png'),(50,'586c884169f8e.png'),(50,'586c88418dc74.png'),(50,'586c8841b8902.png'),(68,'589541badb56b.png'),(68,'589541bad838a.png'),(68,'589541bacb609.png'),(53,'586c94e0416da.png'),(54,'586c9571de53d.png'),(55,'587122518756c.png'),(55,'587122524e17a.png'),(55,'58712254d1420.png'),(70,'589544888dcb9.png'),(70,'5895448858db4.png'),(70,'589544884de0f.png'),(5,'588041f37f380.png'),(6,'588058af77776.png'),(7,'588066826f1b9.png'),(8,'588069f632aa3.png'),(8,'58806a2aaf3dc.png'),(10,'58806cb56161b.png'),(11,'5880703b270ec.png'),(12,'588d24c529c9c.png'),(13,'588d2e80ccf8b.png'),(14,'588d300755c0a.png'),(15,'588d310c2bbcc.png'),(16,'588d3316bd9f4.png'),(17,'588d348c9f5bd.png'),(18,'588d36eea345c.png'),(19,'588d39b56466e.png'),(20,'588d3abcd53cb.png'),(21,'588d3d204406b.png'),(71,'58957e686cbec.png'),(71,'58957e4f4d15f.png'),(71,'58957e4ccde5b.png'),(72,'589580095b08a.png'),(72,'5895800dc6431.png'),(72,'589580155a9a1.png'),(73,'58958183857a0.png'),(73,'589581841cb17.png'),(73,'589581849a3e6.png'),(74,'58958313dc8df.png'),(74,'5895831e6f4ba.png'),(74,'589583308deec.png'),(76,'5895856a257f8.png'),(76,'5895856aad6ee.png'),(76,'5895856e00cee.png'),(75,'586c1e35a7dbb.png'),(75,'586c1e9394258.png');
/*!40000 ALTER TABLE `Post_Images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Post_Rate`
--

DROP TABLE IF EXISTS `Post_Rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Post_Rate` (
  `Post_ID` int(11) NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Rate` decimal(11,0) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Post_Rate`
--

LOCK TABLES `Post_Rate` WRITE;
/*!40000 ALTER TABLE `Post_Rate` DISABLE KEYS */;
INSERT INTO `Post_Rate` VALUES (1,1,3),(1,1,3),(73,47,4),(64,47,4),(75,47,2),(76,47,4),(70,47,4),(72,47,3),(55,37,4),(71,47,4),(51,45,4),(51,47,4);
/*!40000 ALTER TABLE `Post_Rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Post_Views`
--

DROP TABLE IF EXISTS `Post_Views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Post_Views` (
  `Post_ID` int(111) NOT NULL,
  `User_ID` int(111) NOT NULL,
  `View_Number` int(111) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Post_Views`
--

LOCK TABLES `Post_Views` WRITE;
/*!40000 ALTER TABLE `Post_Views` DISABLE KEYS */;
INSERT INTO `Post_Views` VALUES (74,47,1),(51,1,2),(75,1,1),(51,1,1),(75,47,1),(76,47,3),(73,47,1),(65,47,1),(64,47,1),(61,47,1),(59,47,1),(60,47,1);
/*!40000 ALTER TABLE `Post_Views` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Posts`
--

DROP TABLE IF EXISTS `Posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Posts` (
  `Post_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Shop_Name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `Post_Title` varchar(150) CHARACTER SET utf8 NOT NULL,
  `Post_Desc` text CHARACTER SET utf8 NOT NULL,
  `Sale_Start_Date` date NOT NULL,
  `Sale_End_Date` date NOT NULL,
  `Sale_Qty` int(11) NOT NULL,
  `Post_Image` text CHARACTER SET utf8 NOT NULL,
  `Confirm` int(11) NOT NULL,
  `Price_Before` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Price_After` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Post_Time` varchar(150) NOT NULL,
  `User_ID` int(111) NOT NULL,
  `Offer` int(111) NOT NULL,
  PRIMARY KEY (`Post_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=latin1 COMMENT='جدول البوستات التى تنشر بواسطه صاحب المحل';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Posts`
--

LOCK TABLES `Posts` WRITE;
/*!40000 ALTER TABLE `Posts` DISABLE KEYS */;
INSERT INTO `Posts` VALUES (57,'fdddddddddd','hkgyilkyfiloyfi','sdddddddddfwegt3wrgaew','0000-00-00','0000-00-00',5,'5892be031e695.png',0,'150','100','5252',0,0),(58,'fdddddddddd','hkgyilkyfiloyfi','sdddddddddfwegt3wrgaew','0000-00-00','0000-00-00',5,'5892be308d08e.png',0,'150','100','5252',0,0),(61,'adidas','عرض بداية العام','اشتري حذاء اديداس الاصلي بميت جنيه بس باقي عدد قليل من القطع','2017-01-01','2017-01-26',20,'58951a7c7472d.png',0,'150','100','1483507773506',0,0),(68,'ابو علي ','adidas chose','new sale to adidas chose','2017-02-04','2017-02-28',5,'58955dbf1eb92.png',0,'250','100','1486183844566',47,0),(64,'adidas','عرض بداية العام','اشتري حذاء اديداس الاصلي بميت جنيه بس باقي عدد قليل من القطع','2017-01-01','2017-01-26',20,'589523f1e41af.png',0,'150','100','1483507773506',0,0),(65,'adidas','عرض بداية العام','اشتري حذاء اديداس الاصلي بميت جنيه بس باقي عدد قليل من القطع','2017-01-01','2017-01-26',20,'58952488b2b00.png',1,'150','100','1483507773506',555,0),(73,'ابو علي ','ةيةي','ياا','2017-02-04','2017-02-04',0,'5895817e217b9.png',1,'646','99','1486193020545',47,1),(74,'ابو علي ','ىىىى','ىىىى','1900-02-01','2017-02-04',0,'589582c8a554e.png',1,'5','5','1486193304293',47,0),(75,'ابو علي ','ىىىى','ىىىى','1900-02-01','2017-02-04',0,'589582f218a3d.png',2,'5','5','1486193364720',47,0);
/*!40000 ALTER TABLE `Posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rate`
--

DROP TABLE IF EXISTS `Rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rate` (
  `Rate_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Post_ID` int(11) NOT NULL,
  `Rate_Count` varchar(10) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`Rate_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rate`
--

LOCK TABLES `Rate` WRITE;
/*!40000 ALTER TABLE `Rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Shops`
--

DROP TABLE IF EXISTS `Shops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Shops` (
  `Shop_ID` int(11) NOT NULL AUTO_INCREMENT,
  `User_ID` int(11) NOT NULL,
  `Shop_Name` varchar(150) NOT NULL,
  `Shop_Address` varchar(250) NOT NULL,
  `Shop_Logo` text NOT NULL,
  `Shop_Work_TimeFrom` varchar(150) NOT NULL,
  `Shop_Work_TimeTo` varchar(250) NOT NULL,
  `Shop_Work_TimeFrom2` varchar(250) NOT NULL,
  `Shop_Work_TimeTo2` varchar(250) NOT NULL,
  `Cat_ID` int(11) NOT NULL,
  PRIMARY KEY (`Shop_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='جدول بيانات المحلات المضافه للتطبيق';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Shops`
--

LOCK TABLES `Shops` WRITE;
/*!40000 ALTER TABLE `Shops` DISABLE KEYS */;
INSERT INTO `Shops` VALUES (28,1,'dfbzdgbdg','hhhhhhhhhhhhhhh','5892bc41478e4.png','525','','122222222','',1),(29,33,'polo t-shirts','طنطا برده','586c889ab1ce5.png','من الصبح اليلبليل','','','',1),(30,34,'مطعم','الممر','586c93f911a4b.png','من ٥الي١٠','','','',2),(31,34,'اتتااا','اتت','586eee350bf51.png','من ٦الي٦','','','',2),(32,35,'a','a','586f12f970a13.png','من 3الي2','','','',2),(33,35,'b','b','587121ba548f0.png','من 1الي5','','','',2),(34,35,'ريفن','السلطان حسين _الاسماعيلية ','587134bb4a2f9.png','من 3الي2','','','',1),(35,36,'vsvs','vsvs','58800893bd394.png','من 6الي5','','','',1),(36,36,'g','g','588014c29b509.png','من 6:21 PMالي6:21 PM','','','',1),(38,36,'test','tanta','5894c78ebc0c8.png','21311652478:9 PM21311653217:9 AM','','21311652472131165321','',1),(39,1,'77777777777777777777777777777777','ajudvhcuaidk','58951c4d5714d.png','10','5','6','12',2),(40,47,'طيبه ','السلطان حسين','58957da917181.png','6:0 AM','2:0 PM','2:30 PM','11:0 PM',1);
/*!40000 ALTER TABLE `Shops` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `User_ID` int(11) NOT NULL AUTO_INCREMENT,
  `User_Name` varchar(150) DEFAULT NULL,
  `User_Email` varchar(100) DEFAULT NULL,
  `User_Password` varchar(150) DEFAULT NULL,
  `User_Image` text,
  `User_Phone` varchar(250) NOT NULL,
  PRIMARY KEY (`User_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='جدول بيانات المستخدمين داخل التطبيق';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (32,'عبدالرحمن','aa@gmail.com','123456','586b2aa0cd2e7.png','537427583'),(33,'111111111111111111111','testing@gmail.com','fdffd','fdsfsdfsdf','75373793'),(34,'محمود','a@gmail.com','123','586c92aba1f88.png','73739+/'),(35,'ahmed','ahmedboghdady6@gmail.com','12345','586f12b5ef4ea.png','6/968389'),(36,'test','test@gmail.com','12345','587ff937ad0cb.png','8689635'),(37,'111111111111111111111','testing@gmail.com','fdffd','5893652bc2056.png','22222'),(38,'شسس','  testing@gmail.com','12345','588f485e29e5f.png','863868'),(39,'nadccm','testing@gmail.com','fdffd','588f4a47f093c.png','8638638'),(40,'nadccmbb','testing@gmail.com','fdffd','588f4a787f816.png','83893'),(41,'Abdulrahman Gamal','abdo@gmail.com','12345','588f6687b14cf.png','8389+69'),(42,'ahmed','boghdady@gmail.com','12345','58909ca04f6cc.png','8638'),(43,'ahmed','boggd','12345','58909d1249582.png','752769'),(44,'Omar Osama','omar@gmail.com','12345','5890cbe9220a5.png','75896'),(45,'testapp','testapp@gmail.com','12345','589105896134f.png','578'),(46,'dfuahdf','sdfs@','12558','58928552a154c.png','042586'),(47,'محمد احمد','mohamed@gmail.com','12345','5893acc35bf12.png','0101444455');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-10 15:41:35
