<?php
  require_once('utilities/common.php');
  require_once('utilities/dbconfig.php');
  require_once('utilities/html.php');

  $admin = getAdmin($_GET['id']);

  if($_SESSION['uid'] != 1 && $_SESSION['uid'] != $_GET['id'])
    echo "<script>alert('لا تمتلك الصلاحيات لتنفيذ هذه العملية');window.location.href='alladmins.php'</script>";

  if($_POST['editadmin'] == 'editadmin'){
    addAdmin($_GET['id']);
  }
?>

<?php 
echo file_get_contents('head.php');
?>
<body class="skin-blue sidebar-mini" dir="rtl">
    <div class="wrapper">
    <?php
    echo file_get_contents('navbar.php');
    echo file_get_contents('sidebar.php');
    ?>

    <div class="content-wrapper" style="min-height: 921px;">

      
            <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            الإدارة
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <!-- /.box -->

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">إضافة مدير</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <form method="post" enctype="multipart/form-data" class="form-horizontal">

                <input type="hidden" value="editadmin" name="editadmin" />
                                  <div class="box-body">
                                    
                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">إسم المدير</label>
                                      <div class="col-sm-8">
                                        <input
                                        name="name" required class="form-control"  placeholder="العنوان">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">الباسوورد</label>
                                      <div class="col-sm-8">
                                        <input
                                        name="password" required class="form-control"  placeholder="العنوان">
                                      </div>
                                    </div>

                                  </div><!-- /.box-body -->
                                  <div class="box-footer">
                                    <a class="btn btn-default" href="allbekia.php">إلغاء</a>
                                    <button type="submit" class="btn btn-info">حفظ</button>
                                  </div><!-- /.box-footer -->
                                </form>                </div><!-- /.box-body -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
      <!-- Left side column. contains the logo and sidebar -->
      

      <!-- Content Wrapper. Contains page content -->
      <!-- /.content-wrapper -->
      

      <!-- Control Sidebar -->
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg" style="position: fixed; height: auto;"></div>
      <?php
      echo file_get_contents('footer.php');
      ?>
    </div><!-- ./wrapper -->
    
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->


    <script src="plugins/input-mask/jquery.inputmask.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script src="plugins/numericInput.min.js"></script>

    <script>

    $(document).ready(function(){
      $("#startdatemask").inputmask("dd/mm/yyyy", {'placeholder': ''});
      $("#enddatemask").inputmask("dd/mm/yyyy", {'placeholder': ''});
      $(".num").numericInput();
    });
    </script>
</div>
</body>
