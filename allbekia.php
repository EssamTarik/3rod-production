<?php
  require_once('utilities/common.php');
  require_once('utilities/dbconfig.php');
  require_once('utilities/html.php');
?>

<?php 
echo file_get_contents('head.php');
?>
<body class="skin-blue sidebar-mini" dir="rtl">
    <div class="wrapper">
    <?php
    echo file_get_contents('navbar.php');
    echo file_get_contents('sidebar.php');
    ?>

    <div class="content-wrapper" style="min-height: 921px;">

      
            <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            البيكيا
          </h1>
          <hr>

         
          <br>
          <center>
            <a href="addbekia.php">
            <button class="btn btn-default">إضافة بيكيا <i class="fa fa-fw fa-plus"></i> </button>
            </a>

            <a href="deleteallbekia.php">
            <button class="btn btn-default">حذف الكل <i class="fa fa-fw fa-remove"></i></button>
            </a>

            <a href="deletependingbekia.php">
            <button class="btn btn-default">حذف بانتظار الموافقة <i class="fa fa-fw fa-remove"></i></button>
            </a>

            <a href="deleteapprovedbekia.php">
            <button class="btn btn-default">حذف تمت الموافقة <i class="fa fa-fw fa-remove"></i></button>
            </a>

            <a href="deletedeclinedbekia.php">
            <button class="btn btn-default">حذف المرفوض <i class="fa fa-fw fa-remove"></i></button>
            </a>
            </center>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <!-- /.box -->

              <div class="box">
                <div class="box-header">
                <center>
                <table class="table table-bordered table-hover">
                
                <tr>
                  <td>عدد البيكيا بانتظار الموافقة</td>
                  <td>عدد البيكيا تمت الموافقة عليها</td>
                  <td>عدد البيكيا المرفوضة</td>
                </tr>
                <tr>
                  <td><?php echo countBekia()[0]; ?></td>
                  <td><?php echo countBekia()[1]; ?></td>
                  <td><?php echo countBekia()[2]; ?></td>
                </tr>

                </table>
                </center>
                </div><!-- /.box-header -->
                <br>
                <div class="box-body">
                <table id="example1" class="table table-bordered table-hover">
                                   <thead>
                                     <tr>
                                       <th>عنوان البيكيا</th>
                                       <th>تاريخ البيكيا</th>
                                       <th>التأكيد</th>
                                       <th>السعر</th>
                                       <th>الصورة</th>
                                       <th>عمليات</th>
                                     </tr>
                                   </thead>
                                   <tbody>
                                    
                                    <?php
                                     readAllBekia();
                                    ?>
                                   
                                   </tbody>
                                   <tfoot>
                                     <tr>
                                     <th>عنوان البيكيا</th>
                                     <th>تاريخ البيكيا</th>
                                     <th>التأكيد</th>
                                     <th>السعر</th>
                                     <th>الصورة</th>
                                     <th>عمليات</th>                                     </tr>
                                   </tfoot>
                                 </table>
                               </div><!-- /.box-body -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
      <!-- Left side column. contains the logo and sidebar -->
      

      <!-- Content Wrapper. Contains page content -->
      <!-- /.content-wrapper -->
      

      <!-- Control Sidebar -->
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg" style="position: fixed; height: auto;"></div>
      <?php
      echo file_get_contents('footer.php');
      ?>
    </div><!-- ./wrapper -->
    
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $('#example1').DataTable({paging: false, order: []});
      });
    </script>
  
</div>
</body>
