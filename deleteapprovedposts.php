<?php
  require_once('utilities/common.php');
  require_once('utilities/dbconfig.php');
  require_once('utilities/html.php');
  if($_POST['delete'] == 'delete'){
    deleteApprovedPosts();
    header('location: allposts.php');
  }
?>

<?php 
echo file_get_contents('head.php');
?>
<body class="skin-blue sidebar-mini" dir="rtl">
    <div class="wrapper">
    <?php
    echo file_get_contents('navbar.php');
    echo file_get_contents('sidebar.php');
    ?>

    <div class="content-wrapper" style="min-height: 921px;">

      
            <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            البوستات
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <!-- /.box -->

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">حذف بوست</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post">
                    <input type="hidden" name="delete" value="delete" />
                    حذف البوستات التي تمت الموافقة عليها؟
                    <br>
                    <div class="box-footer">
                    <button type="submit" class="btn btn-danger pull-right btn-block">تأكيد</button>
                    <a href="allposts.php" class="btn btn-primary pull-right btn-block">إلغاء</a>
                    </div>
                    </form>

                </div><!-- /.box-body -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
      <!-- Left side column. contains the logo and sidebar -->
      

      <!-- Content Wrapper. Contains page content -->
      <!-- /.content-wrapper -->
      

      <!-- Control Sidebar -->
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg" style="position: fixed; height: auto;"></div>
      <?php
      echo file_get_contents('footer.php');
      ?>
    </div><!-- ./wrapper -->
    
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $('#example1').DataTable({paging: false});
      });
    </script>
  
</div>
</body>
