<?php
  require_once('utilities/common.php');
  require_once('utilities/dbconfig.php');
  require_once('utilities/html.php');

  $bekia = getBekia($_GET['id']);
  $postImages = readBekiaImages($_GET['id']);

  if($_POST['editbekia'] == 'editbekia'){
    editBekia($_GET['id']);
  }
?>

<?php 
echo file_get_contents('head.php');
?>
<body class="skin-blue sidebar-mini" dir="rtl">
    <div class="wrapper">
    <?php
    echo file_get_contents('navbar.php');
    echo file_get_contents('sidebar.php');
    ?>

    <div class="content-wrapper" style="min-height: 921px;">

      
            <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            البيكيا
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <!-- /.box -->

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">تعديل بيكيا</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <form method="post" enctype="multipart/form-data" class="form-horizontal">

                <input type="hidden" value="editbekia" name="editbekia" />
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">عنوان البيكيا</label>
                                      <div class="col-sm-8">
                                        <input value='<?php echo $bekia->Bekia_Title;?>'
                                        name="title" required class="form-control"  placeholder="العنوان">
                                      </div>
                                    </div>


                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">وصف البيكيا</label>
                                      <div class="col-sm-8">
                                        <textarea class="form-control" name="desc" placeholder="وصف البيكيا"><?php echo $bekia->Bekia_Desc;?></textarea>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">التاريخ</label>
                                      <div class="col-sm-8">
                                        <input value='<?php
                                        echo $bekia->Bekia_Date;
                                        ?>'
                                        name="date" id="startdatemask" required class="form-control"  placeholder="التاريخ">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">المستخدم</label>
                                      <div class="col-sm-8">
                                      <select name="user" class="form-control">
                                          <?php
                                            readUsersAsOptionsWithSelected($bekia->User_ID);
                                          ?>
                                      </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">الحالة</label>
                                      <div class="col-sm-8">
                                      <select name="confirm" class="form-control">
                                      <?php
                                      	for($i=0; $i<3; $i++){
                                      		$texts = array('بانتظار الموافقة', 'تمت الموافقة', 'مرفوض');

                                      		if($i == $bekia->Confirm)
                                      			echo "<option selected value='$i'>$texts[$i]</option>";
                                      		else 
                                      			echo "<option value='$i'>$texts[$i]</option>";
                                      	}

                                      ?>
                                      </select>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">السعر</label>
                                      <div class="col-sm-8">
                                        <input value='<?php echo $bekia->Price;?>'
                                        name="price" type="number" required class="form-control num" id="num">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">صورة البوست</label><?php echo $postImages[0]; ?>
                                      <div class="col-sm-8">
                                        <input name="image" type="file" class="form-control">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">صورة البوست</label><?php echo $postImages[1]; ?>
                                      <div class="col-sm-8">
                                        <input name="image2" type="file" class="form-control">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label  class="col-sm-3 control-label">صورة البوست</label><?php echo $postImages[2]; ?>
                                      <div class="col-sm-8">
                                        <input name="image3" type="file" class="form-control">
                                      </div>
                                    </div>


                                  </div><!-- /.box-body -->
                                  <div class="box-footer">
                                    <a class="btn btn-default" href="allbekia.php">إلغاء</a>
                                    <button type="submit" class="btn btn-info">حفظ</button>
                                  </div><!-- /.box-footer -->
                                </form>                </div><!-- /.box-body -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
      <!-- Left side column. contains the logo and sidebar -->
      

      <!-- Content Wrapper. Contains page content -->
      <!-- /.content-wrapper -->
      

      <!-- Control Sidebar -->
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg" style="position: fixed; height: auto;"></div>
      <?php
      echo file_get_contents('footer.php');
      ?>
    </div><!-- ./wrapper -->
    
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->


    <script src="plugins/input-mask/jquery.inputmask.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script src="plugins/numericInput.min.js"></script>

    <script>

    $(document).ready(function(){
      $("#startdatemask").inputmask("dd/mm/yyyy", {'placeholder': ''});
      $("#enddatemask").inputmask("dd/mm/yyyy", {'placeholder': ''});
      $(".num").numericInput();
    });
    </script>
</div>
</body>
