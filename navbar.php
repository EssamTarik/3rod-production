<header class="main-header">
        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>Index</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Index</b>soft</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              
              
              
              
              
              
              <!-- User Account: style can be found in dropdown.less -->
            <br>
            <a href="logout.php" class="btn btn-primary btn-flat">تسجيل الخروج</a>

              <!-- Control Sidebar Toggle Button -->
              
            </ul>
          </div>
        </nav>
      </header>