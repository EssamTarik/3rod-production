<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="height: auto;">
          <!-- Sidebar user panel -->

          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">القائمة الرئيسية</li>
            <li class="treeview">
              <a href="allposts.php">
                <i class="fa fa-dashboard"></i> <span>البوستات</span> <i class="fa fa-angle-left pull-right" style=""></i>
              </a>
            </li>
             
            <li class="treeview">
              <a href="allusers.php">
                <i class="fa fa-dashboard"></i> <span>المستخدمين</span> <i class="fa fa-angle-left pull-right" style=""></i>
              </a>
            </li>

            <li class="treeview">
              <a href="allnews.php">
                <i class="fa fa-dashboard"></i> <span>الأخبار</span> <i class="fa fa-angle-left pull-right" style=""></i>
              </a>
            </li>

             <li class="treeview">
              <a href="allshops.php">
                <i class="fa fa-dashboard"></i> <span>المحلات</span> <i class="fa fa-angle-left pull-right" style=""></i>
              </a>
            </li>

            <li class="treeview">
              <a href="allbekia.php">
                <i class="fa fa-dashboard"></i> <span>البيكيا</span> <i class="fa fa-angle-left pull-right" style=""></i>
              </a>
            </li>

            <li class="treeview">
              <a href="alladmins.php">
                <i class="fa fa-dashboard"></i> <span>الإدارة</span> <i class="fa fa-angle-left pull-right" style=""></i>
              </a>
            </li>


        </section>
        <!-- /.sidebar -->
      </aside>