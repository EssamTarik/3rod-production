<?php
require_once('utilities/dbconfig.php');

session_start();

function isLoggedIn(){
        
        
        if(isset($_SESSION['uid']) && getAdmin($_SESSION['uid'])){
        
                return true;
        
        }else if(isset($_COOKIE['uid']) && getAdmin($_COOKIE['uid'])){
        
                $_SESSION['uid'] = $_COOKIE['uid'];
                return true;
        
        }
        return false;
}

function login(){
        global $loginError;
        $user = adminExists($_POST['username'], $_POST['password']);
        
        if($user){
                $_SESSION['uid'] = $user->Admin_ID;
                
                if(isset($_POST['remember']))
                        setcookie(
                          "uid",
                          $user->Admin_ID,
                          time() + (10 * 365 * 24 * 60 * 60)
                        );
                else
                        setcookie(
                          "uid",
                          null,
                          time() - (10 * 365 * 24 * 60 * 60)
                        );

                header('location:index.php');
                }
        else{
                $loginError=1;
        }
}


function readAllBekia(){
        //read all posts
        $bekias = getAllBekia();
        foreach($bekias as $bekia){
                echo "<tr>";
                write($bekia->Bekia_Title, 'td');
                write($bekia->Bekia_Date, 'td');
                                
                $js = "

                    var ajaxurl = 'confirmBekia.php?confirm=';
                    ajaxurl +=($(this).val());
                    ajaxurl += '&id=';
                    ajaxurl += $bekia->Bekia_ID;
                    $.ajax({url: ajaxurl});
                    ";

                echo '<td><select onChange="'. $js .'" name="confirm" class="form-control">';

                  for($i=0; $i<3; $i++){
                      $texts = array('بانتظار الموافقة', 'تمت الموافقة', 'مرفوض');

                      if($i == $bekia->Confirm)
                          echo "<option selected value='$i'>$texts[$i]</option>";
                      else 
                          echo "<option value='$i'>$texts[$i]</option>";
                  }
                echo '</select></td>';

                write($bekia->Price, 'td');
                $image = "<center><img src='../images/".$bekia->Bekia_Image."' width='50' height='50' /></center>";
                write($image, 'td');
                
                $actions = "<center>";

                $actions .= "<a class='btn btn-primary pull-right btn-block btn-sm' href='editbekia.php?id=".$bekia->Bekia_ID."'>تعديل</a>";

                $actions .="<br />";

                $actions .= "<a class='btn btn-danger pull-right btn-block btn-sm' href='deletebekia.php?id=".$bekia->Bekia_ID."'>حذف</a>";

                $actions .="</center>";
                write($actions, 'td');

                echo "</tr>";
        }
}

function readAllShops(){
        //read all posts
        $shops = getAllShops();
        foreach($shops as $shop){
                echo "<tr>";
                write($shop->Shop_Name, 'td');
                write($shop->Shop_Address, 'td');
                write(getUser($shop->User_ID)->User_Name, 'td');
                write($shop->Shop_Work_TimeFrom, 'td');
                write($shop->Shop_Work_TimeTo, 'td');
                write(getCatigory($shop->Cat_ID)->Cat_Name, 'td');

                $image = "<center><img src='../images/".$shop->Shop_Logo."' width='50' height='50' /></center>";
                write($image, 'td');
                
                $actions = "<center>";

                $actions .= "<a class='btn btn-primary pull-right btn-block btn-sm' href='editshop.php?id=".$shop->Shop_ID."'>تعديل</a>";

                $actions .="<br />";

                $actions .= "<a class='btn btn-danger pull-right btn-block btn-sm' href='deleteshop.php?id=".$shop->Shop_ID."'>حذف</a>";

                $actions .="</center>";
                write($actions, 'td');

                echo "</tr>";
        }
}


function readAllAdmins(){
        //read all posts
        $admins = getAllAdmins();
        foreach($admins as $admin){
                echo "<tr>";
                write($admin->Admin_User_Name, 'td');
                $actions = "<center>";

                $actions .= "<a class='btn btn-primary pull-right btn-block btn-sm' href='editadmin.php?id=".$admin->Admin_ID."'>تعديل</a>";

                $actions .="<br />";

                $actions .= "<a class='btn btn-danger pull-right btn-block btn-sm' href='deleteadmin.php?id=".$admin->Admin_ID."'>حذف</a>";

                $actions .="</center>";
                if($_SESSION['uid'] == 1 || $admin->Admin_ID == $_SESSION['uid'])
                   $actions = $actions; 
                else
                    $actions = "";
                    
                write($actions, 'td');

                echo "</tr>";
        }
}


function readAllPosts(){
        //read all posts
        $posts = getAllPosts();
        foreach($posts as $post){
                echo "<tr>";
                write($post->Shop_Name, 'td');
                write($post->Post_Title, 'td');
                
                $offer = 'عادي';
                
                if((int)$post->Offer == 1)
                    $offer = 'مميز';

                write($offer, 'td');
                write(getUser($post->User_ID)->User_Name, 'td');
                write(getPostViews($post->Post_ID), 'td');
                

                $js = "

                    var ajaxurl = 'confirmPost.php?confirm=';
                    ajaxurl +=($(this).val());
                    ajaxurl += '&id=';
                    ajaxurl += $post->Post_ID;
                    $.ajax({url: ajaxurl});
                    ";

                echo '<td><select onChange="'. $js .'" name="confirm" class="form-control">';

                  for($i=0; $i<3; $i++){
                      $texts = array('بانتظار الموافقة', 'تمت الموافقة', 'مرفوض');

                      if($i == $post->Confirm)
                          echo "<option selected value='$i'>$texts[$i]</option>";
                      else 
                          echo "<option value='$i'>$texts[$i]</option>";
                  }
                echo '</select></td>';

                $image = "<center><img src='../images/".$post->Post_Image."' width='50' height='50' /></center>";
                write($image, 'td');
                
                $actions = "<center>";

                $actions .= "<a class='btn btn-primary pull-right btn-block btn-sm' href='editpost.php?id=".$post->Post_ID."'>تعديل</a>";

                $actions .="<br />";

                $actions .= "<a class='btn btn-danger pull-right btn-block btn-sm' href='deletepost.php?id=".$post->Post_ID."'>حذف</a>";

                $actions .="</center>";
                write($actions, 'td');

                echo "</tr>";
        }
}




function readAllNews(){
        //read all posts
        $newss = getAllNews();
        foreach($newss as $news){
                echo "<tr>";
                write($news->News_Title, 'td');
                write($news->News_Date, 'td');

                $image = "<center><img src='../images/".$news->News_Image."' width='50' height='50' /></center>";
                write($image, 'td');
                
                $actions = "<center>";

                $actions .= "<a class='btn btn-primary pull-right btn-block btn-sm' href='editnews.php?id=".$news->News_ID."'>تعديل</a>";

                $actions .="<br />";

                $actions .= "<a class='btn btn-danger pull-right btn-block btn-sm' href='deletenews.php?id=".$news->News_ID."'>حذف</a>";

                $actions .="</center>";
                write($actions, 'td');

                echo "</tr>";
        }
}



function readAllUsers(){
        //read all posts
        $users = getAllUsers();
        foreach($users as $user){
                echo "<tr>";
                write($user->User_Name, 'td');
                write($user->User_Email, 'td');
                write($user->User_Password, 'td');
                write($user->User_Phone, 'td');
                $image = "<center><img src='../images/".$user->User_Image."' width='50' height='50' /></center>";
                write($image, 'td');
                
                $actions = "<center>";

                $actions .= "<a class='btn btn-primary pull-right btn-block btn-sm' href='edituser.php?id=".$user->User_ID."'>تعديل</a>";

                $actions .="<br />";

                $actions .= "<a class='btn btn-danger pull-right btn-block btn-sm' href='deleteuser.php?id=".$user->User_ID."'>حذف</a>";

                $actions .="</center>";
                write($actions, 'td');

                echo "</tr>";
        }
}

function readPostImages($id){
    $images = array();

    foreach (getPostImages($id) as $image) {
        $imageHTML = "<image width='50' height='50' src='../images/";
        $imageHTML .= $image->Image;
        $imageHTML .= "' />";
        $images[] = $imageHTML;
    }
    return $images;

}

function readBekiaImages($id){
    $images = array();

    foreach (getBekiaImages($id) as $image) {
        $imageHTML = "<image width='50' height='50' src='../images/";
        $imageHTML .= $image->Image;
        $imageHTML .= "' />";
        $images[] = $imageHTML;
    }
    return $images;

}

function readPendingPosts(){
        //read all posts
        $posts = getPendingPosts();
        foreach($posts as $post){
                echo "<tr>";
                write($post->Shop_Name, 'td');
                write($post->Post_Title, 'td');
                write(getPostViews($post->Post_ID), 'td');
                switch($post->Confirm){
                        case 0:
                                write('بإنتظار الموافقة', 'td');
                                break;
                        case 1:
                                write('تمت الموافقة', 'td');
                                break;
                        case 2:
                                write('مرفوض', 'td');
                                break;
                }

                $image = "<center><img src='../images/".$post->Post_Image."' width='50' height='50' /></center>";
                write($image, 'td');

                $actions = "<center>";

                $actions .= "<a class='btn btn-primary pull-right btn-block btn-sm' href='editpost.php?id=".$post->Post_ID."'>تعديل</a>";

                $actions .="<br />";

                $actions .= "<a class='btn btn-danger pull-right btn-block btn-sm' href='deletepost.php?id=".$post->Post_ID."'>حذف</a>";

                $actions .="</center>";
                write($actions, 'td');

                echo "</tr>";
        }
}

function readApprovedPosts(){
        //read all posts
        $posts = getApprovedPosts();
        foreach($posts as $post){
                echo "<tr>";
                write($post->Shop_Name, 'td');
                write($post->Post_Title, 'td');
                write(getPostViews($post->Post_ID), 'td');
                switch($post->Confirm){
                        case 0:
                                write('بإنتظار الموافقة', 'td');
                                break;
                        case 1:
                                write('تمت الموافقة', 'td');
                                break;
                        case 2:
                                write('مرفوض', 'td');
                                break;
                }

                $image = "<center><img src='../images/".$post->Post_Image."' width='50' height='50' /></center>";
                write($image, 'td');

                $actions = "<center>";

                $actions .= "<a class='btn btn-primary pull-right btn-block btn-sm' href='editpost.php?id=".$post->Post_ID."'>تعديل</a>";

                $actions .="<br />";

                $actions .= "<a class='btn btn-danger pull-right btn-block btn-sm' href='deletepost.php?id=".$post->Post_ID."'>حذف</a>";

                $actions .="</center>";
                write($actions, 'td');

                echo "</tr>";
        }
}

function readDeclinedPosts(){
        //read all posts
        $posts = getDeclinedPosts();
        foreach($posts as $post){
                echo "<tr>";
                write($post->Shop_Name, 'td');
                write($post->Post_Title, 'td');
                write(getPostViews($post->Post_ID), 'td');
                switch($post->Confirm){
                        case 0:
                                write('بإنتظار الموافقة', 'td');
                                break;
                        case 1:
                                write('تمت الموافقة', 'td');
                                break;
                        case 2:
                                write('مرفوض', 'td');
                                break;
                }

                $image = "<center><img src='../images/".$post->Post_Image."' width='50' height='50' /></center>";
                write($image, 'td');

                $actions = "<center>";

                $actions .= "<a class='btn btn-primary pull-right btn-block btn-sm' href='editpost.php?id=".$post->Post_ID."'>تعديل</a>";

                $actions .="<br />";

                $actions .= "<a class='btn btn-danger pull-right btn-block btn-sm' href='deletepost.php?id=".$post->Post_ID."'>حذف</a>";

                $actions .="</center>";
                write($actions, 'td');

                echo "</tr>";
        }
}

function readUsersAsOptions(){
    foreach (getAllUsers() as $user) {
      echo "<option value='$user->User_ID'>$user->User_Name</option>";
    }
}

function readCatsAsOptions(){
    foreach (getAllCats() as $cat) {
      echo "<option value='$cat->Cat_ID'>$cat->Cat_Name</option>";
    }
}

function readUsersAsOptionsWithSelected($id){
    foreach (getAllUsers() as $user) {
        if($user->User_ID == $id)
            echo "<option selected value='$user->User_ID'>$user->User_Name</option>";
        else
            echo "<option value='$user->User_ID'>$user->User_Name</option>";
    }
}


function readCatsAsOptionsWithSelected($id){
    foreach (getAllCats() as $cat) {
        if($cat->Cat_ID == $id)
            echo "<option selected value='$cat->Cat_ID'>$cat->Cat_Name</option>";
        else
            echo "<option value='$cat->Cat_ID'>$cat->Cat_Name</option>";
    }
}


if(!isLoggedIn() && !isset($insideLoginPage))
        header('location: login.php');
?>