<?php

session_start();

$pdo = new PDO('mysql:host=localhost;dbname=3rod;charset=utf8', 'root', 'scriptus');

function addPost(){
	echo "<pre>";
	global $pdo;

	$query = $pdo->prepare("insert into Posts set 
			Post_Title = ?,
			Shop_Name = ?,
			Post_Desc = ?,
			Sale_Start_Date = ?,
			Sale_End_Date = ?,
			Sale_Qty = ?,
			Post_Image = ?,
			Confirm = ?,
			Price_Before = ?,
			Price_After = ?,
			Post_Time = ?,
			User_ID = ?,
			Offer = ?
			;
		");
	$image = $_FILES['image'];
	$image2 = $_FILES['image2'];
	$image3 = $_FILES['image3'];

	move_uploaded_file($image['tmp_name'], '../images/'.$image['name']);
	move_uploaded_file($image2['tmp_name'], '../images/'.$image2['name']);
	move_uploaded_file($image3['tmp_name'], '../images/'.$image3['name']);

	$data = array();

	$startdate = \DateTime::createFromFormat('d/m/Y', $_POST['startdate']);
	$enddate = \DateTime::createFromFormat('d/m/Y', $_POST['enddate']);
	
	if($startdate && $enddate){
	
		$startdate = $startdate->format('Y-m-d');
		$enddate = $enddate->format('Y-m-d');
	}else{
		die('من فضلك أدخل تواريخاً صالحة <button onClick="window.history.back()">رجوع</a>');
	}



	$data[] = $_POST['title'];
	$data[] = $_POST['shop'];
	$data[] = $_POST['desc'];
	$data[] = $startdate;
	$data[] = $enddate;
	$data[] = $_POST['qty'];
	$data[] = $image['name'];
	$data[] = $_POST['confirm'];
	$data[] = $_POST['pricebefore'];
	$data[] = $_POST['priceafter'];
	$data[] = time();
	$data[] = $_POST['user'];
	$data[] = $_POST['offer'];

	var_dump($data);

	try{
		$success = $query->execute($data);
		$id = $pdo->lastInsertId();

		foreach (array($image, $image2, $image3) as $imageFile) {
			
			$query = $pdo->prepare("
				insert into Post_Images set
				Post_ID = $id,
				Image = ?
			");
			$query->execute(array($imageFile['name']));
		}

		header('location: allposts.php');
	}catch(Exception $e){

	}

}


function addNews(){
	echo "<pre>";
	global $pdo;

	$query = $pdo->prepare("insert into News set 
			News_Title = ?,
			News_Desc = ?,
			News_Date = ?,
			News_Time = ?,
			Admin_ID = ?,
			News_Image = ?
			;
		");

	$image = $_FILES['image'];

	move_uploaded_file($image['tmp_name'], '../images/'.$image['name']);
	$data = array();

	$date = \DateTime::createFromFormat('d/m/Y', $_POST['date']);
	
	if($date){	
		$date = $date->format('Y-m-d');
	}else{
		die('من فضلك أدخل تواريخاً صالحة <button onClick="window.history.back()">رجوع</a>');
	}
	

	$data[] = $_POST['title'];
	$data[] = $_POST['desc'];
	$data[] = $date;
	$data[] = time();
	$data[] = $_SESSION['uid'];
	$data[] = $image['name'];

	var_dump($data);

	try{
		$success = $query->execute($data);
		header('location: allnews.php');
	}catch(Exception $e){

	}

}


function editNews($id){
	echo "<pre>";
	global $pdo;

	$query = $pdo->prepare("update News set 
			News_Title = ?,
			News_Desc = ?,
			News_Date = ?,
			News_Time = ?,
			Admin_ID = ?,
			News_Image = ?
			where News_Id=$id;
			;
		");

	$image = $_FILES['image'];

	if(!empty($image['name']))
		move_uploaded_file($image['tmp_name'], '../images/'.$image['name']);
	else
		$image = array('name' => getOneNews($id)->News_Image);

	move_uploaded_file($image['tmp_name'], '../images/'.$image['name']);
	$data = array();

	$date = \DateTime::createFromFormat('d/m/Y', $_POST['date']);
	
	if($date){	
		$date = $date->format('Y-m-d');
	}else{
		die('من فضلك أدخل تواريخاً صالحة <button onClick="window.history.back()">رجوع</a>');
	}
	

	$data[] = $_POST['title'];
	$data[] = $_POST['desc'];
	$data[] = $date;
	$data[] = time();
	$data[] = $_SESSION['uid'];
	$data[] = $image['name'];

	var_dump($data);

	try{
		$success = $query->execute($data);
		header('location: allnews.php');
	}catch(Exception $e){

	}

}


function addBekia(){
	echo "<pre>";
	global $pdo;

	$query = $pdo->prepare("insert into Bekia set 
			Bekia_Title = ?,
			Bekia_Desc = ?,
			Bekia_Date = ?,
			Price = ?,
			Bekia_Image = ?,
			Confirm = ?,
			Bekia_Time = ?,
			User_ID = ?
			;
		");
	$image = $_FILES['image'];
	$image2 = $_FILES['image2'];
	$image3 = $_FILES['image3'];

	move_uploaded_file($image['tmp_name'], '../images/'.$image['name']);
	move_uploaded_file($image2['tmp_name'], '../images/'.$image2['name']);
	move_uploaded_file($image3['tmp_name'], '../images/'.$image3['name']);

	$data = array();
	

	$data[] = $_POST['title'];
	$data[] = $_POST['desc'];
	$data[] = $_POST['date'];
	$data[] = $_POST['price'];
	$data[] = $image['name'];
	$data[] = $_POST['confirm'];
	$data[] = time();
	$data[] = $_POST['user'];

	var_dump($data);

	try{
		$success = $query->execute($data);
		var_dump($query->errorInfo());
		$id = $pdo->lastInsertId();

		foreach (array($image, $image2, $image3) as $imageFile) {
			
			$query = $pdo->prepare("
				insert into Bekia_Image set
				Bekia_ID = $id,
				Image = ?
			");
			$query->execute(array($imageFile['name']));
			var_dump($query->errorInfo());
		}
		header('location: allbekia.php');
	}catch(Exception $e){

	}

}

function getPost($id){
	global $pdo;

	$query = $pdo->prepare("select * from Posts where Post_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	return $res[0];	
}

function getPostImages($id){
	global $pdo;

	$query = $pdo->prepare("select * from Post_Images where Post_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	return $res;
}

function getBekiaImages($id){
	global $pdo;

	$query = $pdo->prepare("select * from Bekia_Image where Bekia_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	return $res;	
}


function countPosts(){
	global $pdo;
	$postCount = array();

	$query = $pdo->prepare("select count(Post_ID) from Posts where Confirm=0");
	$query->execute();

	$res = $query->fetchAll(PDO::FETCH_ASSOC);

	$postCount[0] = $res[0]['count(Post_ID)'];	


	$query = $pdo->prepare("select count(Post_ID) from Posts where Confirm=1");
	$query->execute();

	$res = $query->fetchAll(PDO::FETCH_ASSOC);

	$postCount[1] = $res[0]['count(Post_ID)'];


	$query = $pdo->prepare("select count(Post_ID) from Posts where Confirm=2");
	$query->execute();

	$res = $query->fetchAll(PDO::FETCH_ASSOC);

	$postCount[2] = $res[0]['count(Post_ID)'];	

	return $postCount;
}

function countUsers(){
	global $pdo;

	$query = $pdo->prepare("select count(User_ID) from Users");
	$query->execute();

	$res = $query->fetchAll(PDO::FETCH_ASSOC);

	$postCount = $res[0]['count(User_ID)'];	

	return $postCount;
}


function countShops(){
	global $pdo;

	$query = $pdo->prepare("select count(Shop_ID) from Shops");
	$query->execute();

	$res = $query->fetchAll(PDO::FETCH_ASSOC);

	$postCount = $res[0]['count(Shop_ID)'];	

	return $postCount;
}


function countBekia(){
	global $pdo;
	$postCount = array();

	$query = $pdo->prepare("select count(Bekia_ID) from Bekia where Confirm=0");
	$query->execute();

	$res = $query->fetchAll(PDO::FETCH_ASSOC);

	$postCount[0] = $res[0]['count(Bekia_ID)'];	


	$query = $pdo->prepare("select count(Bekia_ID) from Bekia where Confirm=1");
	$query->execute();

	$res = $query->fetchAll(PDO::FETCH_ASSOC);

	$postCount[1] = $res[0]['count(Bekia_ID)'];


	$query = $pdo->prepare("select count(Bekia_ID) from Bekia where Confirm=2");
	$query->execute();

	$res = $query->fetchAll(PDO::FETCH_ASSOC);

	$postCount[2] = $res[0]['count(Bekia_ID)'];	

	return $postCount;
}


function getNews($id){
	global $pdo;

	$query = $pdo->prepare("select * from News where News_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	return $res[0];	
}

function getShop($id){
	global $pdo;

	$query = $pdo->prepare("select * from Shops where Shop_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	return $res[0];	
}


function getBekia($id){
	global $pdo;

	$query = $pdo->prepare("select * from Bekia where Bekia_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	return $res[0];	
}

function getCatigory($id){
	global $pdo;

	$query = $pdo->prepare("select * from Catigory where Cat_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	return $res[0];	
}


function getOneNews($id){
	global $pdo;

	$query = $pdo->prepare("select * from News where News_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	return $res[0];	
}


function getUser($id){
	global $pdo;

	$query = $pdo->prepare("select * from Users where User_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	if(!empty($res))
		return $res[0];
	else
		return false;
}


function getAdmin($id){
	global $pdo;

	$query = $pdo->prepare("select * from Admins where Admin_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	if(!empty($res))
		return $res[0];
	else
		return false;
}




function getCurrentUser(){
	global $pdo;
	$id = $_SESSION['uid'];
	$query = $pdo->prepare("select * from Users where User_ID=?");
	$query->execute(array($id));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	if(!empty($res))
		return $res[0];
	else
		return false;
}



function userExists($email, $password){
	global $pdo;
	$query = $pdo->prepare("select * from Users where User_Email=? and User_Password=?");
	$query->execute(array($email, $password));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	if(empty($res))
		return false;
	else
		return $res[0];
}

function adminExists($username, $password){
	global $pdo;
	$query = $pdo->prepare("select * from Admins where Admin_User_Name=? and Admin_Password=?");
	$query->execute(array($username, $password));
	$res = $query->fetchAll(PDO::FETCH_CLASS);

	if(empty($res))
		return false;
	else
		return $res[0];
}

function editPost($id){
	$id = (int)$id;
	global $pdo;

	$query = $pdo->prepare("update Posts set 
			Post_Title = ?,
			Shop_Name = ?,
			Post_Desc = ?,
			Sale_Start_Date = ?,
			Sale_End_Date = ?,
			Sale_Qty = ?,
			Post_Image = ?,
			Confirm = ?,
			Price_Before = ?,
			Price_After = ?,
			Post_Time = ?,
			User_ID = ?,
			Offer = ?
			where Post_ID=$id
			;
		");
	$image = $_FILES['image'];
	$image2 = $_FILES['image2'];
	$image3 = $_FILES['image3'];

	$images = getPostImages($id);

	$i = 0;
	foreach(array($image, $image2, $image3) as $imageFile){
		if(!empty($imageFile['name'])){
			move_uploaded_file($imageFile['tmp_name'], '../images/'.$imageFile['name']);
			$imageQuery = $pdo->prepare("update Post_Images set
				Image = ?
				where Post_ID = ? and Image = ? limit 1;
			");
			$imageQuery->execute(array($imageFile['name'], $id, $images[$i]->Image));
		}
		$i++;
	}
	

	if(!empty($image['name']))
		move_uploaded_file($image['tmp_name'], '../images/'.$image['name']);
	else
		$image = array('name' => getPost($id)->Post_Image);

	$data = array();

	$startdate = \DateTime::createFromFormat('d/m/Y', $_POST['startdate']);
	$enddate = \DateTime::createFromFormat('d/m/Y', $_POST['enddate']);
	
	if($startdate && $enddate){
	
		$startdate = $startdate->format('Y-m-d');
		$enddate = $enddate->format('Y-m-d');
	}else{
		die('من فضلك أدخل تواريخاً صالحة <button onClick="window.history.back()">رجوع</a>');
	}
	

	$data[] = $_POST['title'];
	$data[] = $_POST['shop'];
	$data[] = $_POST['desc'];
	$data[] = $startdate;
	$data[] = $enddate;
	$data[] = $_POST['qty'];
	$data[] = $image['name'];
	$data[] = $_POST['confirm'];
	$data[] = $_POST['pricebefore'];
	$data[] = $_POST['priceafter'];
	$data[] = time();
	$data[] = $_POST['user'];
	$data[] = $_POST['offer'];

	echo "<pre>";

	try{
		$success = $query->execute($data);
		header('location: allposts.php');
	}catch(Exception $e){
	}

}

function editBekia($id){
	$id = (int)$id;
	global $pdo;

	$query = $pdo->prepare("update Bekia set 
			Bekia_Title = ?,
			Bekia_Desc = ?,
			Bekia_Date = ?,
			Price = ?,
			Bekia_Image = ?,
			Confirm = ?,
			Bekia_Time = ?,
			User_ID = ?
			where Bekia_ID=$id
			;
		");

	$image = $_FILES['image'];
	$image2 = $_FILES['image2'];
	$image3 = $_FILES['image3'];

	$images = getBekiaImages($id);

	$i = 0;
	foreach(array($image, $image2, $image3) as $imageFile){
		if(!empty($imageFile['name'])){
			move_uploaded_file($imageFile['tmp_name'], '../images/'.$imageFile['name']);
			$imageQuery = $pdo->prepare("update Bekia_Image set
				Image = ?
				where Bekia_ID = ? and Image = ? limit 1;
			");
			$imageQuery->execute(array($imageFile['name'], $id, $images[$i]->Image));
		}
		$i++;
	}
	
	if(!empty($image['name']))
		move_uploaded_file($image['tmp_name'], '../images/'.$image['name']);
	else
		$image = array('name' => getBekia($id)->Bekia_Image);

	$data = array();
	

	$data[] = $_POST['title'];
	$data[] = $_POST['desc'];
	$data[] = $_POST['date'];
	$data[] = $_POST['price'];
	$data[] = $image['name'];
	$data[] = $_POST['confirm'];
	$data[] = time();
	$data[] = $_POST['user'];

	echo "<pre>";

	try{
		$success = $query->execute($data);
		var_dump($query->errorInfo());
		header('location: allbekia.php');
	}catch(Exception $e){
	}

}


function editShop($id){
	$id = (int)$id;
	global $pdo;

	$query = $pdo->prepare("update Shops set 
			Shop_Name = ?,
			Shop_Address = ?,
			Shop_Logo = ?,
			Shop_Work_TimeFrom = ?,
			Shop_Work_TimeTo = ?,
			User_ID = ?,
			Cat_ID = ?
			where Shop_ID=$id
			;
		");
	$image = $_FILES['image'];

	if(!empty($image['name']))
		move_uploaded_file($image['tmp_name'], '../images/'.$image['name']);
	else
		$image = array('name' => getShop($id)->Shop_Logo);

	$data = array();
	

	$data[] = $_POST['name'];
	$data[] = $_POST['address'];
	$data[] = $image['name'];
	$data[] = $_POST['timefrom'];
	$data[] = $_POST['timeto'];
	$data[] = $_POST['user'];
	$data[] = $_POST['catid'];

	echo "<pre>";

	try{
		$success = $query->execute($data);
		var_dump($query->errorInfo());
		header('location: allshops.php');
	}catch(Exception $e){
	}

}


function addShop(){
	global $pdo;

	$query = $pdo->prepare("insert into Shops set 
			Shop_Name = ?,
			Shop_Address = ?,
			Shop_Logo = ?,
			Shop_Work_TimeFrom = ?,
			Shop_Work_TimeTo = ?,
			User_ID = ?,
			Cat_ID = ?
			;
		");
	$image = $_FILES['image'];

	move_uploaded_file($image['tmp_name'], '../images/'.$image['name']);

	$data = array();
	

	$data[] = $_POST['name'];
	$data[] = $_POST['address'];
	$data[] = $image['name'];
	$data[] = $_POST['timefrom'];
	$data[] = $_POST['timeto'];
	$data[] = $_POST['user'];
	$data[] = $_POST['catid'];

	echo "<pre>";

	try{
		$success = $query->execute($data);
		var_dump($query->errorInfo());
		header('location: allshops.php');
	}catch(Exception $e){
	}

}


function editUser($id){
	$id = (int)$id;
	global $pdo;

	$query = $pdo->prepare("update Users set 
			User_Name = ?
			where User_ID=$id
			;
		");

	echo "<pre>";

	try{
		$success = $query->execute(array($_POST['name']));
		var_dump($query->errorInfo());
		header('location: allusers.php');
	}catch(Exception $e){
	}

}






function editAdmin($id){
	$id = (int)$id;
	global $pdo;

	$query = $pdo->prepare("update Admins set 
			Admin_User_Name = ?,
			Admin_Password = ?
			where Admin_ID=$id
			;
		");

	echo "<pre>";

	try{
		$success = $query->execute(array($_POST['name'], $_POST['password']));
		var_dump($query->errorInfo());
		header('location: alladmins.php');
	}catch(Exception $e){
	}

}


function addAdmin($id){
	$id = (int)$id;
	global $pdo;

	$query = $pdo->prepare("insert into Admins set 
			Admin_User_Name = ?,
			Admin_Password = ?
			;
		");

	echo "<pre>";

	try{
		$success = $query->execute(array($_POST['name'], $_POST['password']));
		var_dump($query->errorInfo());
		header('location: alladmins.php');
	}catch(Exception $e){
	}

}


function deletePost($id){
	global $pdo;

	$query = $pdo->prepare("delete from Post_Images where Post_ID=?");
	$query->execute(array($id));

	$query = $pdo->prepare("delete from Posts where Post_ID=?");
	$query->execute(array($id));


	$query = $pdo->prepare("delete from Post_Views where Post_ID=?");
	$query->execute(array($id));


	$query = $pdo->prepare("delete from Post_Rate where Post_ID=?");
	$query->execute(array($id));
}

function deleteBekia($id){
	global $pdo;


	$query = $pdo->prepare("delete from Bekia_Image where Bekia_ID=?");
	$query->execute(array($id));

	$query = $pdo->prepare("delete from Bekia where Bekia_ID=?");
	$query->execute(array($id));
}



function deleteShop($id){
	global $pdo;

	$query = $pdo->prepare("delete from Shops where Shop_ID=?");
	$query->execute(array($id));
}



function deleteAdmin($id){
	global $pdo;

	$query = $pdo->prepare("delete from Admins where Admin_ID=?");
	$query->execute(array($id));
}


function deleteNews($id){
	global $pdo;

	$query = $pdo->prepare("delete from News where News_ID=?");
	$query->execute(array($id));
}

function deleteUser($id){
	global $pdo;

	$query = $pdo->prepare("delete from Users where User_ID=?");
	$query->execute(array($id));
}


function deleteAllPosts(){
	global $pdo;

	$query = $pdo->prepare("delete from Post_Views");
	$query->execute();

	$query = $pdo->prepare("delete from Post_Images");
	$query->execute();
	
	$query = $pdo->prepare("delete from Post_Rate");
	$query->execute();

	$query = $pdo->prepare("delete from Posts");
	$query->execute();
}

function deleteAllBekia(){
	global $pdo;

	$query = $pdo->prepare("delete from Bekia_Image");
	$query->execute();

	$query = $pdo->prepare("delete from Bekia");
	$query->execute();
}


function deleteAllShops(){
	global $pdo;

	$query = $pdo->prepare("delete from Shops");
	$query->execute();
}


function deleteAllAdmins(){
	global $pdo;

	$query = $pdo->prepare("delete from Admins where Admin_ID <> 1");
	$query->execute();
}


function deleteAllNews(){
	global $pdo;

	$query = $pdo->prepare("delete from News");
	$query->execute();
}


function deleteAllUsers(){
	global $pdo;

	$query = $pdo->prepare("delete from Users");
	$query->execute();
}


function deleteDeclinedPosts(){
	global $pdo;

	$query = $pdo->prepare("delete from Post_Views where Post_ID in (select Post_ID from Posts where Confirm=2)");
	$query->execute();

	$query = $pdo->prepare("delete from Post_Rate where Post_ID in (select Post_ID from Posts where Confirm=2)");
	$query->execute();

	$query = $pdo->prepare("delete from Posts where Confirm=2");
	$query->execute();
}


function deleteDeclinedBekia(){
	global $pdo;

	$query = $pdo->prepare("delete from Bekia where Confirm=2");
	$query->execute();
}

function deleteApprovedPosts(){
	global $pdo;

	$query = $pdo->prepare("delete from Post_Views where Post_ID in (select Post_ID from Posts where Confirm=1)");
	$query->execute();

	$query = $pdo->prepare("delete from Post_Rate where Post_ID in (select Post_ID from Posts where Confirm=1)");
	$query->execute();

	$query = $pdo->prepare("delete from Posts where Confirm=1");
	$query->execute();
}

function deleteApprovedBekia(){
	global $pdo;

	$query = $pdo->prepare("delete from Bekia where Confirm=1");
	$query->execute();
}


function deletePendingPosts(){
	global $pdo;

	$query = $pdo->prepare("delete from Post_Views where Post_ID in (select Post_ID from Posts where Confirm=0)");
	$query->execute();

	$query = $pdo->prepare("delete from Post_Rate where Post_ID in (select Post_ID from Posts where Confirm=0)");
	$query->execute();


	$query = $pdo->prepare("delete from Posts where Confirm=0");
	$query->execute();
}


function deletePendingBekia(){
	global $pdo;

	$query = $pdo->prepare("delete from Bekia where Confirm=0");
	$query->execute();
}


function getAllPosts(){
	global $pdo;
	$posts = array();
	$query = $pdo->prepare('select * from Posts order by Post_ID desc');
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_CLASS);
	foreach($res as $row){

		$posts[] = $row;
	};

	return $posts;
}


function getAllCats(){
	global $pdo;
	$posts = array();
	$query = $pdo->prepare('select * from Catigory order by Cat_ID desc');
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_CLASS);
	foreach($res as $row){

		$posts[] = $row;
	};

	return $posts;
}

function getAllShops(){
	global $pdo;
	$posts = array();
	$query = $pdo->prepare('select * from Shops order by Shop_ID desc');
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_CLASS);
	foreach($res as $row){

		$posts[] = $row;
	};

	return $posts;
}


function getAllAdmins(){
	global $pdo;
	$posts = array();
	$query = $pdo->prepare('select * from Admins');
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_CLASS);
	foreach($res as $row){

		$posts[] = $row;
	};

	return $posts;
}


function getAllNews(){
	global $pdo;
	$news = array();
	$query = $pdo->prepare('select * from News order by News_ID desc');
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_CLASS);
	foreach($res as $row){

		$news[] = $row;
	};

	return $news;
}


function getAllUsers(){
	global $pdo;
	$posts = array();
	$query = $pdo->prepare('select * from Users order by User_ID DESC');
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_CLASS);
	foreach($res as $row){

		$posts[] = $row;
	};

	return $posts;
}


function getAllBekia(){
	global $pdo;
	$bekias = array();
	$query = $pdo->prepare('select * from Bekia order by Bekia_ID DESC');
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_CLASS);
	foreach($res as $row){

		$bekias[] = $row;
	};

	return $bekias;
}


function getPendingPosts(){
	global $pdo;
	$posts = array();
	$query = $pdo->prepare('select * from Posts where Confirm=0');
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_CLASS);
	foreach($res as $row){
		
		$posts[] = $row;
	};

	return $posts;
}

function getApprovedPosts(){
	global $pdo;
	$posts = array();
	$query = $pdo->prepare('select * from Posts where Confirm=1');
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_CLASS);
	foreach($res as $row){
		
		$posts[] = $row;
	};

	return $posts;
}

function getDeclinedPosts(){
	global $pdo;
	$posts = array();
	$query = $pdo->prepare('select * from Posts where Confirm=2');
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_CLASS);
	foreach($res as $row){
		
		$posts[] = $row;
	};

	return $posts;
}


function getPostViews($id){
	global $pdo;
	$query = $pdo->prepare("select count(Post_ID) from Post_Views where Post_ID=$id;");
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_ASSOC);
	$res = $res[0]['count(Post_ID)'];
	return $res;
}

function confirmPost($id, $confirm){
	$id = (int)$id;
	global $pdo;

	$query = $pdo->prepare("update Posts set 
			Confirm = ?
			where Post_ID=$id
			;
		");
	$success = $query->execute(array($confirm));
	var_dump($query->errorInfo());	
}

function confirmBekia($id, $confirm){
	$id = (int)$id;
	global $pdo;

	$query = $pdo->prepare("update Bekia set 
			Confirm = ?
			where Bekia_ID=$id
			;
		");
	$success = $query->execute(array($confirm));
	var_dump($query->errorInfo());
	
}


?>