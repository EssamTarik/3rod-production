<?php

	function write($content, $tag){
		$openingTag = "<".$tag.">";
		$closingTag = "</".$tag.">";

		echo $openingTag;
		echo $content;
		echo $closingTag;
	}

	function writeArray($array, $tag){
		$openingTag = "<".$tag.">";
		$closingTag = "</".$tag.">";

		foreach($array as $item){
			echo $openingTag;
			echo $item;
			echo $closingTag;
		}
	}

?>