<?php
session_start();

setcookie(
  "uid",
  null,
  time() - (10 * 365 * 24 * 60 * 60)
);

$_SESSION['uid'] = null;

header('location: login.php');

?>